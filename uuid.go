package util

import "github.com/google/uuid"

//A8uuid gives a uuid string that is probabilistically unique
func A8uuid() string {
	return uuid.Must(uuid.NewRandom()).String()
}