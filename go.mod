module gitlab.com/autom8.network/go-a8-util

go 1.12

require (
	github.com/golang/protobuf v1.3.1
	github.com/google/uuid v1.1.1
	github.com/sirupsen/logrus v1.4.1
	golang.org/x/crypto v0.0.0-20190605123033-f99c8df09eb5
)
