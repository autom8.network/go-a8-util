package util

//TruncateString gives the first 'num' elements of a string, useful for logging data without logging ALL data
func TruncateString(str string, num int) string {
	bnoden := str
	if len(str) > num {
		if num > 3 {
			num -= 3
		}
		bnoden = str[0:num] + "..."
	}
	return bnoden
}
