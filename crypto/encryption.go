package crypto

import (
	"crypto"
	"crypto/aes"
	"crypto/cipher"
	"crypto/md5"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/hex"
	"encoding/pem"
	"fmt"
	a8Util "gitlab.com/autom8.network/go-a8-util"
	"golang.org/x/crypto/sha3"
	"io"
)

var pkBits = 4096

//CreateEncryptedPK creates and then turns to bytes all the necessary key information the CLI needs
func CreateEncryptedPK(password string) (pkPem []byte, encryptedPK []byte, pubBytes []byte, err error) {
	pk, pub := generateKeyPair(pkBits)

	pubBytes = PublicKeyToPem(pub)

	encryptedPK, err = encryptPrivateKey(password, pk)

	pkBytes := privateKeyToPem(pk)

	return pkBytes, encryptedPK, pubBytes, err
}

//SignMessage outputs the signate of a message as signed by the pk
func SignMessage(pk *rsa.PrivateKey, message []byte) ([]byte, error) {
	r := rand.Reader

	hash := sha3.New256()
	_, err := hash.Write(message)
	if err != nil {
		return nil, err
	}
	hashedMessage := hash.Sum(nil)

	psOptions := &rsa.PSSOptions{
		SaltLength: 256,
	}

	result, err := rsa.SignPSS(r, pk, crypto.SHA3_256, hashedMessage, psOptions)
	if err != nil {
		return nil, err
	}

	return result, nil
}

//VerifyMessage verifies a message, signature pair using a public key as PEM
func VerifyMessage(pubString string, message []byte, signature []byte) bool {
	pub, err := PemToPublicKey([]byte(pubString))
	if err != nil {
		return false
	}

	return verifyMessage(pub, message, signature)
}

// PublicKeyToPem public key to pem bytes
func PublicKeyToPem(pub *rsa.PublicKey) []byte {
	pubASN1 := x509.MarshalPKCS1PublicKey(pub)

	pubBytes := pem.EncodeToMemory(&pem.Block{
		Type:  "RSA PUBLIC KEY",
		Bytes: pubASN1,
	})

	return pubBytes
}

//PemToPublicKey pem back to public key
func PemToPublicKey(pemBytes []byte) (*rsa.PublicKey, error) {
	block, _ := pem.Decode(pemBytes)
	if block == nil {
		return nil, fmt.Errorf("no data decoded from input string")
	}

	pub, err := x509.ParsePKCS1PublicKey(block.Bytes)

	if err != nil {
		return nil, err
	}

	return pub, nil
}

// PemToPrivateKey converts a private key PEM to an rsa private key
func PemToPrivateKey(pemPk []byte) (*rsa.PrivateKey, error) {
	block, _ := pem.Decode(pemPk)
	if block == nil {
		return nil, fmt.Errorf("no data decoded from input string")
	}

	pk, err := x509.ParsePKCS1PrivateKey(block.Bytes)
	if err != nil {
		return nil, err
	}

	return pk, nil
}

//DecryptData decrypts two-way encrypted piece of data, mirrors encryptData
func DecryptData(password string, data []byte) ([]byte, error) {
	hashedPw, err := hashData(password)
	if err != nil {
		return nil, err
	}

	block, err := aes.NewCipher([]byte(hashedPw))
	if err != nil {
		return nil, err
	}
	gcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}

	nonceSize := gcm.NonceSize()

	nonce, ciphertext := data[:nonceSize], data[nonceSize:]

	plaintext, err := gcm.Open(nil, nonce, ciphertext, nil)
	if err != nil {
		return nil, err
	}

	return plaintext, nil
}

func encryptPrivateKey(password string, pk *rsa.PrivateKey) ([]byte, error) {
	priBytes := privateKeyToPem(pk)

	encrypted, err := encryptData(priBytes, password)
	if err != nil {
		return nil, err
	}

	return encrypted, nil
}

func verifyMessage(pub *rsa.PublicKey, message []byte, signature []byte) bool {
	hash := sha3.New256() //hashedMessage, []byte(initialMessage))
	_, err := hash.Write(message)
	if err != nil {
		a8Util.Log.Error("couldn't verify message",err)
		return false
	}
	hashedMessage := hash.Sum(nil)

	psOptions := &rsa.PSSOptions{
		SaltLength: 256,
	}

	if err := rsa.VerifyPSS(pub, crypto.SHA3_256, hashedMessage, signature, psOptions); err != nil {
		a8Util.Log.Error("couldn't verify message",err)
		return false
	}

	return true
}

func hashData(key string) (string, error) {
	hasher := md5.New()
	_, err := hasher.Write([]byte(key))
	if err != nil {
		a8Util.Log.Error("error hashing data",err)
		return "", err
	}

	return hex.EncodeToString(hasher.Sum(nil)), nil
}

func encryptData(data []byte, password string) ([]byte, error) {
	hashedPw,err := hashData(password)
	if err != nil {
		return nil, err
	}

	block, err := aes.NewCipher([]byte(hashedPw))
	if err != nil {
		return nil, err
	}
	gcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}

	nonce := make([]byte, gcm.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		panic(err.Error())
	}
	ciphertext := gcm.Seal(nonce, nonce, data, nil)
	return ciphertext, nil

}

// generateKeyPair generates a new key pair
func generateKeyPair(bits int) (*rsa.PrivateKey, *rsa.PublicKey) {
	privkey, err := rsa.GenerateKey(rand.Reader, bits)
	if err != nil {
		a8Util.Log.Error(err)
	}
	return privkey, &privkey.PublicKey
}

func privateKeyToPem(pk *rsa.PrivateKey) []byte {
	ASN1DER := x509.MarshalPKCS1PrivateKey(pk)

	privateBytes := pem.EncodeToMemory(&pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: ASN1DER,
	})

	return privateBytes
}
